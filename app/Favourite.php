<?php

namespace MARVEL;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'favourites';

    /**
     * Atributos que pueden ser llenados por el usuario.
     *
     * @var array
     */
    protected $fillable = [
        'comic_id',
        'name',
        'url_thumbnail',
        'user_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2018/12/04
    Metodos para construir relaciones en ORM
    ******************************************************************/

    // Favourite __belongs_to__ User
    public function user() {
        return $this->belongsTo('MARVEL\User','user_id', 'id');
    }


    public static function isFavourite($comicId, $userId){
        $oFavourite = Favourite::where('comic_id', $comicId)->where('user_id', $userId)->first();
        return is_object($oFavourite) ? TRUE : FALSE;
    }
}
