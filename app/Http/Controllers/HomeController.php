<?php

namespace MARVEL\Http\Controllers;

use Illuminate\Http\Request;

use Ixudra\Curl\Facades\Curl;

use Storage;
use Auth;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Metodo para mostrar la lista de personajes de marvel buscados desde el API de MAVEL
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/06
     *
     * @route /
     * @method GET/POST
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $iPage = 1){
    	
    	$ts = Carbon::now()->timestamp;
    	$publicKey = config('marvel.marvelApiPublicKey');
    	$privateKey = config('marvel.marvelApiPrivateKey');
    	
    	$hash = md5($ts.$privateKey.$publicKey);

    	$iPaginateFilter = 20;
    	if($request->input('iPaginate')){
            $iPaginateFilter = $request->input('iPaginate');
        }else{
            if ($request->session()->has('paginate_temp')) {
                $iPaginateFilter = $request->session()->get('paginate_temp');
            }else{
                $iPaginateFilter = 20;
            }
        }
        $request->session()->flash('paginate_temp', $iPaginateFilter);

    	$sStringSearchFilter = '';
    	if($request->input('sStringSearch')){
            $sStringSearchFilter = $request->input('sStringSearch');
        }else{
            if ($request->session()->has('stringSearch_temp')) {
                $sStringSearchFilter = $request->session()->get('stringSearch_temp');
            }else{
                $sStringSearchFilter = '';
            }
        }
        $request->session()->flash('stringSearch_temp', $sStringSearchFilter);

        $sSortByFilter = '';
        if($request->input('sSortBy')){
            $sSortByFilter = $request->input('sSortBy');
        }else{
            if ($request->session()->has('sortBy_temp')) {
                $sSortByFilter = $request->session()->get('sortBy_temp');
            }else{
                $sSortByFilter = '';
            }
        }
        $request->session()->flash('sortBy_temp', $sSortByFilter);

        $iOffset = $iPage == 1 ? 0 : ($iPage*$iPaginateFilter);

        $aDataWS = [
        	'apikey' => $publicKey,
        	'ts' => $ts,
        	'hash' => $hash,
        	'limit' => $iPaginateFilter,
        	'offset' => $iOffset,
        ];
        if($sStringSearchFilter){
        	$aDataWS['name'] = $sStringSearchFilter;
        }
        if($sSortByFilter){
        	$aDataWS['orderBy'] = $sSortByFilter;	
        }
        
        $response = Curl::to('https://gateway.marvel.com:443/v1/public/characters')
                        ->withData($aDataWS)
                        ->get();

        $aResponse = json_decode($response);

        $totalResults = $aResponse->data->total;
        $iPages = intdiv((int)$totalResults, (int)$iPaginateFilter);
        $rest = $totalResults%$iPaginateFilter;
        if($rest > 0){
        	$iPages++;
        }
        $start = $iOffset+1;
        $end = $start + ($iPaginateFilter - 1);

        if($iPage == 1){
        	$sPrevPageUrl = '#';
        }else{
        	$prevPage = $iPage - 1;
        	$sPrevPageUrl = route('home', [$prevPage]);	
        }

        if($iPage == $iPages){
        	$sNextPageUrl = '#';
        }else{
        	$nextPage = $iPage + 1;
        	$sNextPageUrl = route('home', [$nextPage]);
        }

        $iFirstPage = $iPage <= 3 ? 1 : $iPage - 3 ;
       	$iLastPage = ($iPage + 3) > $iPages ? $iPages : $iPage + 3;

        $aData = [
        	'sStringSearch' => $sStringSearchFilter,
        	'copyright' => $aResponse->copyright,
        	'attributionHTML' => $aResponse->attributionHTML,
            'aCharacters' => $aResponse->data->results,
            'iPaginate' => $iPaginateFilter,
            'iTotalResults' => $totalResults,
            'iPage' => $iPage,
            'iPages' => $iPages,
            'sPrevPageUrl' => $sPrevPageUrl,
            'sNextPageUrl' => $sNextPageUrl,
            'iFirstPage' => $iFirstPage,
            'iLastPage' => $iLastPage,
            'iStart' => $start,
            'iEnd' => $end,
    	];

    	$view = view('home', $aData);
        
        if($request->ajax()){
            $sections = $view->renderSections();
            return response()->json($sections['main-content'], 200);
        }
        
        return $view;
    }


    /**
     * Metodo para mostrar la lista de comics de marvel asociados al personaje seleccionado
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/07
     *
     * @route /comics/{iCharacterId?}
     * @method GET
     * @return \Illuminate\Http\Response
     */
    public function comics(Request $request, $iCharacterId = ''){
    	
    	if( ! $iCharacterId){
    		$aData = [
	        	'copyright' => '',
	        	'attributionHTML' => '',
	            'aComics' => [],
	    	];

	    	$view = view('comics', $aData);
	        
	        if($request->ajax()){
	            $sections = $view->renderSections();
	            return response()->json($sections['main-content'], 200);
	        }
	        
	        return $view;
    	}

    	$ts = Carbon::now()->timestamp;
    	$publicKey = config('marvel.marvelApiPublicKey');
    	$privateKey = config('marvel.marvelApiPrivateKey');
    	
    	$hash = md5($ts.$privateKey.$publicKey);

        $aDataWS = [
        	'apikey' => $publicKey,
        	'ts' => $ts,
        	'hash' => $hash,
        	'limit' => 100,
        ];
        
        $responseCharacter = Curl::to('https://gateway.marvel.com:443/v1/public/characters/'.$iCharacterId)
                        ->withData($aDataWS)
                        ->get();
        $aResponseCharacter = json_decode($responseCharacter);
        
        $response = Curl::to('https://gateway.marvel.com:443/v1/public/characters/'.$iCharacterId.'/comics')
                        ->withData($aDataWS)
                        ->get();
        $aResponse = json_decode($response);

        $aData = [
        	'copyright' => $aResponse->copyright,
        	'attributionHTML' => $aResponse->attributionHTML,
        	'oCharacter' => isset($aResponseCharacter->data->results[0]) ? $aResponseCharacter->data->results[0] : null,
            'aComics' => $aResponse->data->results,
    	];

    	$view = view('comics', $aData);
        
        if($request->ajax()){
            $sections = $view->renderSections();
            return response()->json($sections['main-content'], 200);
        }
        
        return $view;
    }


    /**
     * Metodo para mostrar el detalle de un comic previamente seleccionado por su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/07
     *
     * @route /comics/{iComicId?}
     * @method GET
     * @return \Illuminate\Http\Response
     */
    public function showComic(Request $request, $iComicId = ''){
    	if( ! $iComicId){
    		$aData = [
	            'oComic' => null,
	    	];

	    	return view('comic-detail', $aData);
    	}

    	$ts = Carbon::now()->timestamp;
    	$publicKey = config('marvel.marvelApiPublicKey');
    	$privateKey = config('marvel.marvelApiPrivateKey');
    	
    	$hash = md5($ts.$privateKey.$publicKey);

        $aDataWS = [
        	'apikey' => $publicKey,
        	'ts' => $ts,
        	'hash' => $hash,
        ];
        
        $responseComic = Curl::to('https://gateway.marvel.com:443/v1/public/comics/'.$iComicId)
                        ->withData($aDataWS)
                        ->get();
        $aResponseComic = json_decode($responseComic);

        $aData = [
        	'oComic' => $aResponseComic->data->results[0],
    	];

    	return view('comic-detail', $aData);
    }
}
