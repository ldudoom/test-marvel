<?php

namespace MARVEL\Http\Controllers;

use Illuminate\Http\Request;

use Ixudra\Curl\Facades\Curl;

use MARVEL\User;
use MARVEL\Favourite;

use Auth;
use Carbon\Carbon;

class FavouritesController extends Controller
{
    /**
     * Metodo que devuelve la lista de comics favoritos del usuario
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/06
     *
     * @route /favourites-list
     * @method GET
     * @return \Illuminate\Http\Response
     */
    public function favourites(Request $request){
    	$aFavourites = $request->session()->get('marvel_favourites', []);
    	if(Auth::check()){
    		$favourites = Auth::user()->favourites;
    		if($favourites->isNotEmpty()){
    			$aFavourites = [];
    			foreach($favourites as $oFavourite){
    				$aFavourites[] = [
		    			'comic_id' => $oFavourite->comic_id,
		    			'name' => $oFavourite->name,
		    			'url_thumbnail' => $oFavourite->url_thumbnail,
		    		];
    			}
    		}
    	}
    	if( ! $aFavourites){
    		$offset = rand(0, 43167);
    		$ts = Carbon::now()->timestamp;
	    	$publicKey = config('marvel.marvelApiPublicKey');
	    	$privateKey = config('marvel.marvelApiPrivateKey');
	    	$hash = md5($ts.$privateKey.$publicKey);
	        $aDataWS = [
	        	'apikey' => $publicKey,
	        	'ts' => $ts,
	        	'hash' => $hash,
	        	'limit' => 3,
	        	'offset' => $offset,
	        ];
	        
	        $responseComics = Curl::to('https://gateway.marvel.com:443/v1/public/comics')
	                        ->withData($aDataWS)
	                        ->get();
	        $aResponseComics = json_decode($responseComics);

	        $aComics = $aResponseComics->data->results;
	        if($aComics){
	        	$aFavourites = [];
	        	foreach($aComics as $oComic){
	        		$aFavourites[] = [
		    			'comic_id' => $oComic->id,
		    			'name' => $oComic->title,
		    			'url_thumbnail' => $oComic->thumbnail->path.'.'.$oComic->thumbnail->extension,
		    		];
	        	}
	        }

    	}
    	$data = [
			'favouritesList' => $aFavourites,
		];
    	$aResponse = [
            'htmlContent' => view('favourites', $data)->render(),
		];
		return response()->json($aResponse, 200);
    }

    /**
     * Metodo que agrega un comic a mis favoritos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/06
     *
     * @route /add-favourite
     * @method POST
     * @return \Illuminate\Http\Response
     */
    public function addFavourite(Request $request){
    	if(Auth::check()){
    		$oFavourite = Favourite::where('comic_id', $request->comic_id)->first();
    		if(is_object($oFavourite)){
    			$aResponse = [
    				'status' => false,
    				'code' => 402,
    				'message' => 'You already have this comic in your favorites.',
    			];
    			return response()->json($aResponse, 200);
    		}
    		$oFavourite = new Favourite;
    		$oFavourite->comic_id = $request->comic_id;
	        $oFavourite->name = $request->name;
	        $oFavourite->url_thumbnail = $request->url_thumbnail;
	        $oFavourite->user_id = Auth::user()->id;
	        if($oFavourite->save()){
	        	$aResponse = [
					'status' => true,
					'code' => 200,
					'message' => 'this comic has been added to your favorites',
				];
	        }else{
	        	$aResponse = [
					'status' => false,
					'code' => 401,
					'message' => 'Sorry, this comic could not be added to your favorites',
				];
	        }
    	}else{
    		$aFavourites = $request->session()->get('marvel_favourites', []);
    		if($aFavourites){
    			foreach($aFavourites as $favourite){
    				if($favourite['comic_id'] == $request->comic_id){
    					$aResponse = [
		    				'status' => false,
		    				'code' => 402,
		    				'message' => 'You already have this comic in your favorites.',
		    			];
		    			return response()->json($aResponse, 200);
    				}
    			}
    		}
    		$aFavouritesAux[] = [
    			'comic_id' => $request->comic_id,
    			'name' => $request->name,
    			'url_thumbnail' => $request->url_thumbnail,
    		];
    		$aFavourites = array_merge($aFavourites, $aFavouritesAux);
    		$request->session()->put('marvel_favourites', $aFavourites);
    		$aResponse = [
				'status' => true,
				'code' => 200,
				'message' => 'this comic has been added to your favorites',
			];
    	}
    	return response()->json($aResponse, 200);
    }

    /**
     * Metodo que elimina un comic de mis favoritos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/06
     *
     * @route /remove-favourite
     * @method DELETE
     * @return \Illuminate\Http\Response
     */
    public function deleteFavourite(Request $request, $iComicId = ''){
    	
    	if( ! $iComicId){
    		$aResponse = [
				'status' => false,
				'code' => 402,
				'message' => 'Please select a comic to remove it from your favorites.',
			];
			return response()->json($aResponse, 200);
    	}

    	if(Auth::check()){
    		$oFavourite = Favourite::where('comic_id', $iComicId)->where('user_id', Auth::user()->id)->first();
    		if( ! is_object($oFavourite)){
    			$aResponse = [
    				'status' => false,
    				'code' => 402,
    				'message' => 'The comic you have selected does not exist in your favorites.',
    			];
    			return response()->json($aResponse, 200);
    		}
    		if($oFavourite->delete()){
	        	$aResponse = [
					'status' => true,
					'code' => 200,
					'message' => 'this comic has been deleted from your favorites',
				];
	        }else{
	        	$aResponse = [
					'status' => false,
					'code' => 401,
					'message' => 'Sorry, this comic could not be deleted to your favorites',
				];
	        }
    	}else{
    		$aFavourites = $request->session()->get('marvel_favourites', []);
    		if($aFavourites){
    			foreach($aFavourites as $key => $favourite){
    				if($favourite['comic_id'] == $iComicId){
    					unset($aFavourites[$key]);
    					$request->session()->put('marvel_favourites', $aFavourites);
    					$aResponse = [
							'status' => true,
							'code' => 200,
							'message' => 'this comic has been deleted from your favorites',
						];
						return response()->json($aResponse, 200);
    				}
    			}
    		}
    		$aResponse = [
				'status' => false,
				'code' => 402,
				'message' => 'The comic you have selected does not exist in your favorites..',
			];
			return response()->json($aResponse, 200);
    	}
    }
}
