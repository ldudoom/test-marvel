<?php

namespace MARVEL;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2018/12/07
    Metodos para construir relaciones en ORM
    ******************************************************************/

    // User __has_many__ Favourite
    public function favourites() {
        return $this->hasMany('MARVEL\Favourite','user_id', 'id');
    }

    /**
     * Método que genera el hash de las claves antes de guardarlas en la BD.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/12/07
     *
     * @param string password
     *
     */
    public function setPasswordAttribute($password){
        $this->attributes['password'] = \Hash::make($password);
    }
}
