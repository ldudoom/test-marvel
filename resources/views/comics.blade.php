@extends('layouts.app-layout')

@section('title')
	 Comics
@endsection

@section('main-content')
    <!-- Page Heading -->
    <h3 class="my-4">
    	<img src="{!! asset('public/images/icons/characters.png') !!}" alt="Characters">
    	<small>Comics related to "{!! is_object($oCharacter) ? $oCharacter->name : '' !!}"</small>
      <a href="#" onclick="javascript: renderSection('{!! route('home') !!}', 'mainContentDiv');" class="btn btn-xs btn-danger btn-right">
          << Back to Characters
      </a>
  	</h3>

  	<div class="row">
    
    	@if($aComics)
        	@foreach($aComics as $oComic)
        		<div class="col-md-6 col-sm-6 portfolio-item">
                  <div class="card h-100">
                    <div class="card-body">
                      <img class="img-fluid card-img-top-comic" src="{!! $oComic->thumbnail->path !!}.{!! $oComic->thumbnail->extension !!}" alt="{!! $oComic->title !!}" />
                      <h4 class="card-title">
                        <a href="#" class="text-danger">{!! $oComic->title !!}</a>
                      </h4>
                      <p class="card-text" title="{!! $oComic->description !!}">{!! str_limit($oComic->description, 100, '...') !!}</p>
                      <hr />
                      <a href="#" class="btn btn-sm- btn-danger" onclick="javascript:loadComic('{!! route('comics.detail', [$oComic->id]) !!}');" data-toggle="modal" data-target="#comicDetailModal">VIEW DETAIL</a>
                    </div>
                  </div>
                </div>
        	@endforeach
        @endif
  	</div>
  	<!-- /.row -->
  	

@endsection
