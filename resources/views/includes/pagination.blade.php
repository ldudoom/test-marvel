@if($iTotalResults > 0)
	<div align="center">
		Se muestran del {!! $iStart !!} al {!! $iEnd !!} de un total de {!! $iTotalResults !!}
	</div>
	@if($iTotalResults > $iPaginate)
	  	<ul class="pagination justify-content-center">
	    	<li class="page-item">
	      		<a class="page-link" onclick="javascript: renderSection('{!! $sPrevPageUrl !!}', 'mainContentDiv');" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
	    	</li>
	        @if($iFirstPage > 1)
	        	<li class="page-item">
	          		<a class="page-link" onclick="javascript: renderSection('{!! route('home') !!}', 'mainContentDiv');">1</a>
	            </li>
	        	<li class="page-item">
	        		&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;
	        	</li>
	        @endif

	        @for($i=$iFirstPage;$i<=$iLastPage;$i++)
	            <li class="page-item {!! $iPage == $i ? 'active' : '' !!}">
	              <a class="page-link" onclick="javascript: renderSection('{!! $iPage == $i ? '#' : route('home', [$i]) !!}', 'mainContentDiv');">{!! $i !!}</a>
	            </li>
	        @endfor
	        
	        @php
	        	$iPagesLess = $iPages - 3;	
	        @endphp
	        @if($iLastPage < $iPagesLess)
	        	<li class="page-item">
	        		&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;
	        	</li>
	        	<li class="page-item">
	              <a class="page-link" onclick="javascript: renderSection('{!! route('home', [$iPages]) !!}', 'mainContentDiv');">{!! $iPages !!}</a>
	            </li>
	        @endif
	    
	        <li class="page-item">
	            <a class="page-link" onclick="javascript: renderSection('{!! $sNextPageUrl !!}', 'mainContentDiv');" aria-label="Next">
	            	<span aria-hidden="true">&raquo;</span>
	            	<span class="sr-only">Next</span>
	          	</a>
	        </li>
	  	</ul>
	@endif
@endif