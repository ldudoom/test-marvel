<!-- Bootstrap core JavaScript -->
{!! Html::script('public/vendor/jquery/jquery.min.js', ['type' => 'text/javascript']) !!}
{!! Html::script('public/vendor/bootstrap/js/bootstrap.bundle.min.js', ['type' => 'text/javascript']) !!}
<script type="text/javascript">
	var urlFavourites = '{!! route('favourites') !!}';
</script>
{!! Html::script('public/js/marvel.js') !!}