<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#" onclick="javascript: renderSection('{!! route('home') !!}', 'mainContentDiv');">
        	<img class="img-fluid" style="max-width: 30%;" src="{!! asset('public/images/logo-marvel.png') !!}" />
      	</a>
      	<div class="collapse navbar-collapse" id="navbarResponsive">
          	{!! Form::open(['route' => ['home', 1], 'method' => 'POST', 'name' => 'searchCharactersForm', 'id' => 'searchCharactersForm', 'class' => 'form-inline']) !!}
              <div class="row">
                <div class="col-md-5">
                  {!! Form::text('sStringSearch', old('sStringSearch'), ['id' => 'sStringSearch', 'class' => 'form-control search']) !!}
                </div>
                <div class="col-md-2">
                  {!! Form::select('iPaginate', [5 => 5, 10 => 10, 15 => 15, 20 => 20], 20, ['id' => 'ipaginate', 'class' => 'form-control', 'onChange' => 'javascript: submitSearchForm();']) !!}
                </div>
                <div class="col-md-5">
                  {!! Form::select('sSortBy', ['name' => 'name (a-z)', 'modified' => 'modified (newer first)', '-name' => 'name (z-a)', '-modified' => 'modified (older first)'], null, ['id' => 'sSortBy', 'class' => 'form-control', 'onChange' => 'javascript: submitSearchForm();']) !!}
                </div>
              </div>
            {!! Form::close() !!}
      	</div>
	</div>
</nav>