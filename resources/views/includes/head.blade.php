{!! Html::meta(null, null, ['charset' => 'utf-8']) !!}
{!! Html::meta(null, 'IE=edge', ['http-equiv' => 'X-UA-Compatible']) !!}
{!! Html::meta('viewport', 'width=device-width, initial-scale=1, shrink-to-fit=no') !!}
{!! Html::meta('description', '') !!}
{!! Html::meta('author', 'Raul Alejandro Chauvin Ojeda') !!}

<title>MARVEL :: @yield('title')</title>

<!-- Bootstrap core CSS -->
{!! Html::style('public/vendor/bootstrap/css/bootstrap.min.css') !!}

<!-- Custom styles for this template -->
{!! Html::style('public/css/test-marvel.css') !!}