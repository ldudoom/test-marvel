@extends('layouts.app-layout')

@section('title')
	Inicio
@endsection

@section('main-content')
	<!-- Page Heading -->
	<h3 class="my-4">
    	<img src="{!! asset('public/images/icons/characters.png') !!}" alt="Characters">
    	<small>Characters</small>
  	</h3>

  	<div class="row">
    
    	@if($aCharacters)
        	@foreach($aCharacters as $oCharacter)
        		<div class="col-md-6 col-sm-6 portfolio-item">
                  <div class="card h-100">
                    <div class="card-body">
                      <div class="row">
                      	<div class="col-md-6">
                      		<img class="img-fluid card-img-top" src="{!! $oCharacter->thumbnail->path !!}.{!! $oCharacter->thumbnail->extension !!}" alt="{!! $oCharacter->name !!}" />
                      	</div>
                      	<div class="col-md-6">
                      		<h4 class="card-title">
                        		<a href="#" class="text-danger">{!! $oCharacter->name !!}</a>
                      		</h4>
                      		<p class="card-text" title="{!! $oCharacter->description !!}">{!! str_limit($oCharacter->description, 100, '...') !!}</p>
                      	</div>
                      	<div class="col-md-12">
	                      	@if($oCharacter->comics->available)
	                      		<h4 class="card-title pt-20">
			                        Related Comics
		                      	</h4>
		                      	<div class="row"> 
			                      	@foreach($oCharacter->comics->items as $key => $oComic)
			                      		<div class="col-md-6">
			                      			<small>&bull; {!! $oComic->name !!}</small>
			                      		</div>
			                      		@if($key == 3)
			                      			@php
			                      				break;
			                      			@endphp
			                      		@endif
			                      	@endforeach
		                      	</div>
	                      	@endif
	                      	<hr />
	                      	<a class="btn btn-sm btn-danger" href="#" onclick="javascript: renderSection('{!! route('comics.list', [$oCharacter->id]) !!}', 'mainContentDiv');">VIEW MORE COMICS</a>
                      	</div>
                      </div>
                    </div>
                  </div>
                </div>
        	@endforeach
        @else
        	<div class="alert alert-info">Su búsqueda no produjo resultados.</div>
        @endif
  	</div>
  	<!-- /.row -->
  	
  	<!-- Pagination -->
  	@include('includes.pagination')

@endsection
