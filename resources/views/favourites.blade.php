@if($favouritesList)
	@foreach($favouritesList as $favourite)
		{!! Form::open(['route' => ['favourites.delete', $favourite['comic_id']], 'method' => 'DELETE', 'name' => 'deleteFavouriteForm_'.$favourite['comic_id'], 'id' => 'deleteFavouriteForm_'.$favourite['comic_id']]) !!}
            <img src="{!! asset('public/images/icons/btn-delete.png') !!}" alt="Remove from your Favourites" onclick="javascript: removeFavourites('deleteFavouriteForm_{!! $favourite['comic_id'] !!}');" title="Remove from your Favourites" class="btn-remove-favourite" />
			<img class="img-thumbnail" src="{!! $favourite['url_thumbnail'] !!}" alt="{!! $favourite['name'] !!}">
			<h5>{!! $favourite['name'] !!}</h5>
			<hr />
		{!! Form::close() !!}
	@endforeach
@endif