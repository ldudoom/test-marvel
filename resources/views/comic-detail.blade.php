<div class="modal-header">
  <h5 class="modal-title text-danger" id="exampleModalLabel">{!! $oComic->title !!}</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><img src="{!! asset('public/images/icons/btn-close.png') !!}" alt="close" /></span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
      <div class="col-md-5">
        <img class="img-fluid card-img-top-comic" src="{!! $oComic->thumbnail->path !!}.{!! $oComic->thumbnail->extension !!}" alt="{!! $oComic->title !!}" />
      </div>
      <div class="col-md-7">
        {!! $oComic->description !!}
      </div>
  </div>
</div>
<div class="modal-footer">
  @if(Auth::check())
    @if(MARVEL\Favourite::isFavourite($oComic->id, Auth::user()->id))
      {!! Form::open(['route' => ['favourites.delete', $oComic->id], 'method' => 'DELETE', 'name' => 'deleteFavouriteForm_'.$oComic->id, 'id' => 'deleteFavouriteForm_'.$oComic->id]) !!}
        <button type="button" class="btn btn-block btn-danger" data-dismiss="modal" onclick="javascript: removeFavourites('deleteFavouriteForm_{!! $oComic->id !!}');">
            <img src="{!! asset('public/images/icons/btn-delete.png') !!}" alt="Remove from Favourites" /> Remove from Favourites
        </button>
      {!! Form::close() !!}
    @else
      {!! Form::open(['route' => 'favourites.add', 'method' => 'POST', 'name' => 'addFavouriteForm_'.$oComic->id, 'id' => 'addFavouriteForm_'.$oComic->id]) !!}
        {!! Form::hidden('comic_id', $oComic->id) !!}
        {!! Form::hidden('name', $oComic->title) !!}
        {!! Form::hidden('url_thumbnail', $oComic->thumbnail->path.'.'.$oComic->thumbnail->extension) !!}
        <button type="button" class="btn btn-block btn-info" data-dismiss="modal" onclick="javascript: addFavourites('addFavouriteForm_{!! $oComic->id !!}', '{!! $oComic->title !!}', '{!! $oComic->thumbnail->path !!}.{!! $oComic->thumbnail->extension !!}');">
            <img src="{!! asset('public/images/icons/btn-favourites-default.png') !!}" alt="Add to Favourites" /> Add to Favourites
        </button>
      {!! Form::close() !!}
    @endif
  @else
    @php
      $aFavorites = session('marvel_favourites', []);
      $banFavourite = false;
      if($aFavorites){
        foreach($aFavorites as $favourite){
          if($favourite['comic_id'] == $oComic->id){
            $banFavourite = true;
          }
        }
      }
    @endphp
    @if($banFavourite)
      {!! Form::open(['route' => ['favourites.delete', $oComic->id], 'method' => 'DELETE', 'name' => 'deleteFavouriteForm_'.$oComic->id, 'id' => 'deleteFavouriteForm_'.$oComic->id]) !!}
        <button type="button" class="btn btn-block btn-danger" data-dismiss="modal" onclick="javascript: removeFavourites('deleteFavouriteForm_{!! $oComic->id !!}');">
            <img src="{!! asset('public/images/icons/btn-delete.png') !!}" alt="Remove from Favourites" /> Remove from Favourites
        </button>
      {!! Form::close() !!}
    @else
      {!! Form::open(['route' => 'favourites.add', 'method' => 'POST', 'name' => 'addFavouriteForm_'.$oComic->id, 'id' => 'addFavouriteForm_'.$oComic->id]) !!}
        {!! Form::hidden('comic_id', $oComic->id) !!}
        {!! Form::hidden('name', $oComic->title) !!}
        {!! Form::hidden('url_thumbnail', $oComic->thumbnail->path.'.'.$oComic->thumbnail->extension) !!}
        <button type="button" class="btn btn-block btn-info" data-dismiss="modal" onclick="javascript: addFavourites('addFavouriteForm_{!! $oComic->id !!}', '{!! $oComic->title !!}', '{!! $oComic->thumbnail->path !!}.{!! $oComic->thumbnail->extension !!}');">
            <img src="{!! asset('public/images/icons/btn-favourites-default.png') !!}" alt="Add to Favourites" /> Add to Favourites
        </button>
      {!! Form::close() !!}
    @endif
  @endif
</div>