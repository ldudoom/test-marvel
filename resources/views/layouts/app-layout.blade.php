<!DOCTYPE html>
<html lang="en">

  <head>

    @include('includes.head')

    @yield('custom-css')

  </head>

  <body>

    <!-- Navigation -->
    @include('includes.navbar')

      @include('includes.preloader')

      <!-- Page Content -->
      <div class="container-fuid">

        <div class="row">
          
          <div class="col-md-9 col-sm-12" id="mainContentDiv">
            @yield('main-content')
          </div>

          <div class="col-md-3 col-sm-12 favourites">
            <h3 class="my-4">
              <img src="{!! asset('public/images/icons/favourites.png') !!}" alt="Characters">
              <small>My Favourites</small>
            </h3>
            <div id="favouritesDiv"></div>
          </div>

        </div>

      </div>

      <div class="modal fade" id="comicDetailModal" tabindex="-1" role="dialog" aria-labelledby="MARVEL" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content" id="comicDetailDiv"></div>
        </div>
      </div>
    

    </div>
    <!-- /.container -->

    @include('includes.footer')

    @include('includes.scripts')

    @yield('custom-js')

  </body>

</html>
