$(document).ready(function(){
	$(".preloader").fadeOut();
	loadFavourites();
});

function submitSearchForm(){
	$('#searchCharactersForm').submit();	
}

$('#searchCharactersForm').submit(function(e){
	$.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: "json",
        beforeSend: function(){
          	$(".preloader").show();
        },
        success: function (data) {
            $('#mainContentDiv').empty().append($(data)); 
            $(".preloader").fadeOut(500);
        },
        error: function (data) {
            $(".preloader").fadeOut(500);
            alert('Ups. An error occurred, please try again.');
            var errors = data.responseJSON;
            if (errors) {
                $.each(errors, function (i) {
                    console.log(errors[i]);
                });
            }
        }
    });
	e.preventDefault();
});

function renderSection(url, divId){
	if(url != '#'){
		$.ajax({
	        type: 'GET',
	        url: url,
	        dataType: 'json',
	        beforeSend: function(){
	          	$(".preloader").show();
	        },
	        success: function (data) {
	            $('#'+divId).empty().append($(data)); 
	            $(".preloader").fadeOut(500);
	        },
	        error: function (data) {
	            $(".preloader").fadeOut(500);
	            var errors = data.responseJSON;
	            if (errors) {
	                $.each(errors, function (i) {
	                    console.log(errors[i]);
	                });
	            }
	        }
	    });
	}
}

function loadComic(url){
	$(".preloader").show();
	$('#comicDetailDiv').hide();
	$('#comicDetailDiv').load(url, function(){
		$(".preloader").fadeOut(500);
		$('#comicDetailDiv').fadeIn();
	});
}

function loadFavourites(){
	$.ajax({
        type: 'GET',
        url: urlFavourites,
        dataType: 'json',
        beforeSend: function(){
          	$(".preloader").show();
        },
        success: function (data) {
            $('#favouritesDiv').empty().append(data.htmlContent);
            $(".preloader").fadeOut(500);
        },
        error: function (data) {
            $(".preloader").fadeOut(500);
            var errors = data.responseJSON;
            if (errors) {
                $.each(errors, function (i) {
                    console.log(errors[i]);
                });
            }
        }
    });
}

function addFavourites(formId){
	$.ajax({
        type: $("#"+formId).attr('method'),
        url: $("#"+formId).attr('action'),
        data: $("#"+formId).serialize(),
        dataType: "json",
        beforeSend: function(){
          	$(".preloader").show();
        },
        success: function (data) {
            alert(data.message);
            loadFavourites();
            $(".preloader").fadeOut(500);
        },
        error: function (data) {
            $(".preloader").fadeOut(500);
            alert('Ups. An error occurred saving your favourite, please try again.');
            var errors = data.responseJSON;
            if (errors) {
                $.each(errors, function (i) {
                    console.log(errors[i]);
                });
            }
        }
    });
}

function removeFavourites(formId){
	$.ajax({
        type: $("#"+formId).attr('method'),
        url: $("#"+formId).attr('action'),
        data: $("#"+formId).serialize(),
        dataType: "json",
        beforeSend: function(){
          	$(".preloader").show();
        },
        success: function (data) {
            alert(data.message);
            loadFavourites();
            $(".preloader").fadeOut(500);
        },
        error: function (data) {
            $(".preloader").fadeOut(500);
            alert('Ups. An error occurred deleting your favourite, please try again.');
            var errors = data.responseJSON;
            if (errors) {
                $.each(errors, function (i) {
                    console.log(errors[i]);
                });
            }
        }
    });
}