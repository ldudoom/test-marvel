<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Parametros de API MARVEL
    |--------------------------------------------------------------------------
    |
    |
    */

    // API PUBLIC KEY
    'marvelApiPublicKey' => env('MARVEL_API_PUBLIC_KEY', 'a38028f6c03974eada0f30f59c7b08e4'),

    // API PRIVATE KEY
    'marvelApiPrivateKey' => env('MARVEL_API_PRIVATE_KEY', '7f91f0cd63287bdbf5279f01f6d56d7400cb8e38'),

];
