<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/favourites-list', 'FavouritesController@favourites')->name('favourites');
Route::post('/add-favourite', 'FavouritesController@addFavourite')->name('favourites.add');
Route::delete('/remove-favourite/{iComicId?}', 'FavouritesController@deleteFavourite')->name('favourites.delete');

Route::match(['GET', 'POST'], '/comics/{characterId?}', 'HomeController@comics')->name('comics.list');
Route::get('comic-detail/{comicId?}', 'HomeController@showComic')->name('comics.detail');
Route::match(['GET', 'POST'], '/{iPage?}', 'HomeController@index')->name('home');